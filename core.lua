hideInf = {}

local windows = 
{
	["EA_Window_PublicQuestTracker"] = true,
}

function hideInf.Initialize()
	RegisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED,		 "hideInf.GetZoneData")
	RegisterEventHandler(SystemData.Events.PLAYER_AREA_NAME_CHANGED, "hideInf.GetZoneData")
	RegisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED,  "hideInf.OnLoad")
end

function hideInf.Shutdown()
	UnregisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED,		"hideInf.GetZoneData")
	UnregisterEventHandler(SystemData.Events.PLAYER_AREA_NAME_CHANGED, 	"hideInf.GetZoneData")
	UnregisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED,  	"hideInf.OnLoad")
end

function hideInf.OnLoad()
	hideInf.GetZoneData()
end

function hideInf.GetZoneData()
	local found = false
	for k,v in ipairs(GetAreaData()) do
		if(v.influenceID ~= 0) then
			found = true
			break
		end
	end
	
	if(not found) then
		hideInf.HideWindow()
	else
		hideInf.ShowWindow()
	end
end

function hideInf.ShowWindow()
	for k in pairs(windows) do
		WindowSetShowing(k, true)
	end
end

function hideInf.HideWindow()
	for k in pairs(windows) do
		WindowSetShowing(k, false)
	end
end